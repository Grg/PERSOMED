import tensorflow as tf
import argparse
import sys
import numpy as np
import os
import loader
import util
import collections
import model
import config
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
import config as cfg
from IPython import embed

class Evaluator:

    def __init__(self, model: model.Model, eval_data: loader.DataHolder, eval_indices, experiment_name: str):

        self.model = model

        self.experiment_name = experiment_name
        self.experiment_dir = os.path.abspath(os.path.join("./experiments/", experiment_name))
        self.checkpoint_dir = os.path.abspath(os.path.join("./experiments/", experiment_name, "checkpoint/"))

        self.train_dir = '/'.join([self.experiment_dir, "summaries/train"])
        self.train_writer = tf.summary.FileWriter(self.train_dir)
        self.train_writer.add_graph(graph=tf.get_default_graph())

        self.test_dir = '/'.join([self.experiment_dir, "summaries/test"])
        self.test_writer = tf.summary.FileWriter(self.test_dir)

        self.saver = tf.train.Saver()

        self.preds = tf.nn.softmax(self.model.logits)

        self.eval_data = eval_data
        self.eval_indices = eval_indices
        self.eval_ids = list(range(len(eval_data)))

        self.labels = tf.placeholder(tf.int32, (None,), name="labels")
        self.epoch = tf.placeholder(tf.float32, name="epoch")


    def load_or_initialize(self, session):

        # incijalizacija parametara
        if not os.path.exists(self.checkpoint_dir):
            os.makedirs(self.checkpoint_dir)

        latest_checkpint_path = tf.train.latest_checkpoint(self.checkpoint_dir)
        self.saver.restore(session, latest_checkpint_path)

        tf.get_default_graph().finalize()


    def evaluate(self):

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        config.gpu_options.per_process_gpu_memory_fraction = 0.9

        path = os.path.join(self.experiment_dir, "stage_2_dump.csv")
        with open(path, "w") as f:
            f.write("ID,class1,class2,class3,class4,class5,class6,class7,class8,class9\n")

        with tf.Session(config=config).as_default() as sess:
            self.load_or_initialize(sess)
            # todo: add last smaller batch
            for e_iter in range(0, ((len(self.eval_data)//cfg.batch_size)+1)*cfg.batch_size, cfg.batch_size):
                print("eval_iter:", e_iter)
                # we omit the last batch that is smaller than cfg.batch_size

                eval_batch_data = self.eval_data[e_iter:e_iter+cfg.batch_size]
                eval_batch_data = loader.DataHolder(*eval_batch_data)

                eval_batch_indices = self.eval_indices[e_iter:e_iter+cfg.batch_size]
                eval_batch_ids = self.eval_ids[e_iter:e_iter+cfg.batch_size]

                eval_feed_dict = self.model.embedd_and_create_feed_dict(
                    batch_data=eval_batch_data,
                    batch_best_indices=eval_batch_indices,
                    batch_ids=eval_batch_ids,
                    trainer=self,
                    epoch=0,
                    np_save_dir=cfg.SAVED_EMBEDDED_EVAL_DATA
                )

                preds_e = sess.run(
                        self.preds,
                    feed_dict=eval_feed_dict  # train batch
                )

                self.dump_preds(preds_e, path, start_id=e_iter)

    def dump_preds(self, preds, path, start_id):
        with open(path, "a+") as f:
            for i, l in enumerate(preds):
                f.write(str(start_id + i + 1) + "," + ",".join(map(str, l)) + "\n")


def load_classes(variations_path):
    with open(variations_path) as variations_file:
        classes_names = [var.rstrip().split(",")[-1] for var in variations_file]  # extract classes from file
        classes = list(map(lambda x: int(x)-1, classes_names))

    return classes


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--exp_name', default="test")
    args = parser.parse_args()
    experiment_name = args.exp_name

    if os.path.isfile(cfg.EVAL_PREPROCESSED_PATH):
        print("Loading preprocessed")

        with open(cfg.EVAL_PREPROCESSED_PATH, 'rb') as f:
            eval_data = pickle.load(f)

    else:

        eval_data = loader.DataHolder()

        eval_data.loadData(cfg.EVAL_TEXT_PATH, cfg.EVAL_VARIANTS_PATH)


        print("Data loaded")
        print("Preprocessing")

        tokenizer = util.Tokenizer()
        pos_tagger = util.POSTagger()
        lemmatizer = util.Lematizer()
        alias_supstitutor = util.AliasSubstitutor()
        query_expander = util.QueryExpander()

        preprocessor = util.Preprocessor([alias_supstitutor, tokenizer, pos_tagger, lemmatizer, query_expander])

        eval_data = preprocessor.process(eval_data)

        with open(cfg.EVAL_PREPROCESSED_PATH, 'wb') as f:
            pickle.dump(eval_data, f)

    if os.path.isfile(cfg.TF_IDF_VECTORIZED_PATH):
        with open(cfg.TF_IDF_VECTORIZED_PATH, "rb") as f:
            vectorizer = pickle.load(f)
            floki = util.Retriever(vectorizer)

    else:
        vectorizer = util.TfidfVectorizer(eval_data)  # creates and fits vectorizer
        floki = util.Retriever(vectorizer)

        with open(cfg.TF_IDF_VECTORIZED_PATH, "wb") as f:
            pickle.dump(vectorizer, f)

    print("Retrieving")
    eval_indices = floki.retrieve(eval_data, sentence_count=cfg.SENTENCES_TO_RETRIEVE)

    types = [loader.DataHolder, list, loader.DataHolder, list]

    print("Preprocessing done.")

    model = model.Model(vectorizer=vectorizer, brc_embedding=cfg.BRC_EMBEDDING, sentences_count=cfg.SENTENCES_TO_RETRIEVE)
    evaluator = Evaluator(model, eval_data=eval_data, eval_indices=eval_indices, experiment_name=experiment_name)
    evaluator.evaluate()

