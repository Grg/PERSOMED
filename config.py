import loader
import util
import collections
from enum import Enum

TRAIN_TEXT_PATH = "./data/training_text_r"
TRAIN_VARIANTS_PATH = "./data/training_variants_r"

TEST_TEXT_PATH = "./data/test_text_r"
TEST_VARIANTS_PATH = "./data/test_variants_r"

EVAL_TEXT_PATH = "./data/eval_text_r"
EVAL_VARIANTS_PATH = "./data/eval_variants_r"

ALIASES = "./preprocessed/aliases.txt"

TRAIN_LEMMATIZED_PATH = "./preprocessed/train_lemmatized.pickle"
TEST_LEMMATIZED_PATH = "./preprocessed/test_lemmatized.pickle"
TF_IDF_VECTORIZED_PATH = "./preprocessed/tfidfvectorizer3.pickle"

TEST_PREPROCESSED_PATH = "./preprocessed/test_preprocessed.pickle"
TRAIN_PREPROCESSED_PATH = "./preprocessed/train_preprocessed.pickle"
EVAL_PREPROCESSED_PATH = "./preprocessed/eval_preprocessed.pickle"

# PRETRAINED_EMBEDDINGS = "./pretrained_embeddings/wikipedia-pubmed-and-PMC-w2v.bin"
PRETRAINED_EMBEDDINGS = "./pretrained_embeddings/PubMed-w2v.bin"

SAVED_EMBEDDED_TRAIN_DATA = "./saved_embedded_data/train/"
SAVED_EMBEDDED_TEST_DATA = "./saved_embedded_data/test/"
SAVED_EMBEDDED_EVAL_DATA = "./saved_embedded_data/eval/"

REWRITE_SAVED_EMBEDDED_DATA = False

CLIP = False

SENTENCES_TO_RETRIEVE = 30

TFIDF_SIZE = 75924

BRC_EMBEDDING = True
BRC_BIRNN_HIDDEN_SIZE = 200
BRC_CONV_SIZE = 200

batch_size = 20

MINIMAL_DF = 3

REAL_CONVOLUTION = True
CLASSIFIER = "LOGREG"


assert CLASSIFIER in ["LOGREG", "BRC", "CONV"]


QUERY_EXPANSION_ELEMENTS = ["driver", "passenger"]

IMPORTANT_CHECK_WORDS = ["cancer", "malign", "benign", "pathogenic"]

NEGATIVITY_WORDS = ["can't", "cannot", "mustn't", "don't", "needn't", "shouldn't", "needn't", "not", "no", "none", "nothing", "neither"]

POS_TAGS = ['NOT_FOUND', 'UH', 'SYM', 'CC', 'CD', 'DT', 'EX', 'FW', 'IN', 'JJ', 'JJR', 'JJS', 'LS', 'MD', 'NN', 'NNS', 'NNP', 'NNPS', 'PDT', 'POS', 'PRP', 'PRP$', 'RB', 'RBR', 'RBS', 'RP', 'TO', 'U', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ', 'WDT', 'WP', 'WP$', 'WRB']
