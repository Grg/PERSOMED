import codecs
from nltk import sent_tokenize

class DataHolder:

    def __init__(self, texts=None, queries=None, classes=None):

        self.classes = classes
        self.texts = texts
        self.queries = queries

        self.current = 0
    
    # Reads into memory data from the disk, and build query
    def loadData(self, text_path, variants_path):
        
        qs = []
        classes = []

        with open(variants_path, 'r') as file:
            for line in file:
                split_line = line.strip().split(',')
                classes.append(int(split_line[3]) - 1)  # in the file classes start with 1
                qs.append([split_line[1], split_line[2].lower()])

            self.classes = classes
            self.queries = tuple(qs)

        with open(text_path, "r") as f:
            texts = list(f)
            

        split_texts = [text.split("||")[1] for text in texts]
        new_texts = []

        for text in split_texts:
            sentences = sent_tokenize(text)
            new_texts.append(sentences)
            
        self.texts = new_texts
        
        

    def shuffle(self, indices):
        assert len(indices) == len(self.classes)
        self.classes = [self.classes[i] for i in indices]
        self.texts = [self.texts[i] for i in indices]
        self.queries = [self.queries[i] for i in indices]

    def __iter__(self):
        return self

    def __next__(self):

        if self.current >= len(self.texts):
            self.current = 0
            raise StopIteration

        else:
            item = (self.texts[self.current], self.queries[self.current], self.classes[self.current])
            self.current += 1
            return item

    def __getitem__(self, idx):
        return self.texts[idx], self.queries[idx], self.classes[idx]

    def __len__(self):
        return len(self.texts)


class LightweightDataHolder(DataHolder):

    def __init__(self, length):
        DataHolder.__init__(self)
        self.length = length

    def __iter__(self):
        return self

    def __next__(self):

        if self.current >= self.length:
            self.current = 0
            raise StopIteration

        else:
            return None, None, None

    def __getitem__(self, idx):
        return None, None, None

    def __len__(self):
        return self.length

    def shuffle(self, indices):
        pass

