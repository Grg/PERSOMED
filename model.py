import tensorflow as tf
from tensorflow.contrib.layers import flatten
import loader
import os
import pickle
import numpy as np
import util
from itertools import chain
from tensorflow.contrib import rnn
import gensim
from nltk import pos_tag
from scipy.sparse import coo_matrix, vstack

from functools import reduce
from nltk import word_tokenize
from collections import OrderedDict

import config as cfg

class Model:

    def __init__(self, vectorizer, sentences_count, brc_embedding=True, sentence_sentiments=None):
        with tf.variable_scope("Model"):

            self.vectorizer = vectorizer
            self.brc_embedding = brc_embedding
            self.sentences_count = sentences_count
            self.sentence_sentiments = sentence_sentiments

            if self.brc_embedding:

                self.tag_to_num = {tag: i for i, tag in enumerate(cfg.POS_TAGS)}

                self.w2v = gensim.models.KeyedVectors.load_word2vec_format(
                    cfg.PRETRAINED_EMBEDDINGS,
                    binary=True)

                self.sg_embedding_size = self.w2v.vector_size
                self.full_embedding_size =\
                    self.sg_embedding_size +\
                    len(cfg.QUERY_EXPANSION_ELEMENTS) +\
                    2 * len(cfg.IMPORTANT_CHECK_WORDS) +\
                    1 +\
                    2 +\
                    len(cfg.POS_TAGS)


                self.embedded_words = tf.placeholder(tf.float32, (None, None, self.full_embedding_size), "embedded_words")
                self.sentence_indices = tf.placeholder(tf.int32, (None, None), "sentence_indices")
                self.best_indices = tf.placeholder(tf.float32, (None, None), "best_indices")

                self.vectorized_queries = tf.placeholder(
                    tf.float32, (None, None, self.sg_embedding_size), "vectorized_queries")

            else:
                self.vectorized_text = tf.placeholder(tf.float32, (None, cfg.SENTENCES_TO_RETRIEVE, cfg.TFIDF_SIZE), "embedded_words")
                self.vectorized_queries = tf.placeholder(
                    tf.float32, (None, cfg.TFIDF_SIZE), "vectorized_queries")
 

            self.logits = self.forward_pass()

    def forward_pass(self):

        if self.brc_embedding:
            embedded_sentences = brc_sentence_embed(self.embedded_words, self.sentence_indices)

            vectorized_text = sentence_retrieve(embedded_sentences, self.best_indices)

            vectorized_queries = self.vectorized_queries

        else:
            vectorized_text, vectorized_queries = self.vectorized_text, self.vectorized_queries

        vectorized_text = sentence_sentiment_embedding(vectorized_text, self.sentence_sentiments)

        # we have vectorized_queries and vectorized_text
        logits = classifier(vectorized_text, vectorized_queries, sentence_count=self.sentences_count, num_classes=9, brc=self.brc_embedding)

        return logits

    def one_hot_POS_tag(self, tag):
        num = self.tag_to_num.get(tag, 0)
        oh = np.zeros(shape=(len(cfg.POS_TAGS),))
        oh[num] = 1
        return oh

    def embedd_sentence(self, sentence, query):
        return [np.concatenate((self.embedd_word(token[0], query), self.one_hot_POS_tag(token[1]))) for token in sentence]

    def embedd_query_word(self, word):
        return self.w2v[word] if word in self.w2v.wv.vocab else np.zeros((self.w2v.vector_size,))

    def embedd_word(self, word, query):
        sg = self.w2v[word] if word in self.w2v.wv.vocab else np.zeros((self.w2v.vector_size,))
        assert sg.shape == self.w2v["onion"].shape

        additional_query_features = np.array([word == query_elem for query_elem in query], dtype=int)
        assert len(additional_query_features) == len(query)

        important_words_binary_features = np.array([word == imp_words for imp_words in cfg.IMPORTANT_CHECK_WORDS], dtype=int)
        assert len(important_words_binary_features) == len(cfg.IMPORTANT_CHECK_WORDS)

        important_words_similarity_features = np.array(
            [self.w2v.wv.similarity(word, imp_words) if (word in self.w2v.wv.vocab) else 0.0 for imp_words in cfg.IMPORTANT_CHECK_WORDS])
        assert len(important_words_similarity_features) == len(cfg.IMPORTANT_CHECK_WORDS)

        negativity_feature = np.array([word in cfg.NEGATIVITY_WORDS])

        emb = np.concatenate((sg, additional_query_features, important_words_binary_features,
                              important_words_similarity_features, negativity_feature))

        assert emb.shape[0]+len(cfg.POS_TAGS) == self.full_embedding_size
        return emb

    def return_feed_dict_keys(self, trainer):

        if self.brc_embedding:
            feed_dict_keys = [
                self.embedded_words,
                self.sentence_indices,
                self.vectorized_queries,
                self.best_indices,
                trainer.labels,
                trainer.epoch
            ]

        else:
            feed_dict_keys = [
                self.vectorized_text,
                self.vectorized_queries,
                trainer.labels,
                trainer.epoch
            ]

        return feed_dict_keys


    def embedd_and_create_feed_dict(self, batch_data: loader.DataHolder, batch_best_indices: list,
                                    epoch: int, trainer, batch_ids: list, train=True, np_save_dir=None):

        if self.brc_embedding:

            embedded_docs = list()
            embedded_queries = list()
            docs_sentences_indices = list()
            classes = list()

            if np_save_dir is None:
                np_save_dir = cfg.SAVED_EMBEDDED_TRAIN_DATA if train else cfg.SAVED_EMBEDDED_TEST_DATA

            for id, (doc, query, cls) in zip(batch_ids, batch_data):
                doc_path = os.path.join(np_save_dir, "doc_"+str(id)+".npy")
                query_path = os.path.join(np_save_dir, "query_"+str(id)+".npy")
                indices_path = os.path.join(np_save_dir, "indices_"+str(id)+".npy")
                class_path = os.path.join(np_save_dir, "class_"+str(id)+".npy")

                if os.path.isfile(class_path) and not cfg.REWRITE_SAVED_EMBEDDED_DATA:
                    cls = int(np.load(class_path))

                else:
                    assert cls is not None
                    np.save(class_path, cls)

                classes.append(cls)

                if os.path.isfile(doc_path) and not cfg.REWRITE_SAVED_EMBEDDED_DATA:
                    embedded_doc = np.load(doc_path)

                else:

                    embedded_doc = np.array(list(chain(
                        *[
                            self.embedd_sentence(sentence, query) for sentence in doc]
                    )))

                    np.save(doc_path, embedded_doc.astype(np.float32))

                embedded_docs.append(embedded_doc)

                if os.path.isfile(indices_path) and not cfg.REWRITE_SAVED_EMBEDDED_DATA:
                    sentence_indices = np.load(indices_path)

                else:
                    sentence_lens = [len(sentence) for sentence in doc]

                    sentence_indices = np.cumsum([0]+sentence_lens)[:-1]

                    np.save(indices_path, sentence_indices)

                # padd sentence indices
                docs_sentences_indices.append(sentence_indices)

                if os.path.isfile(query_path) and not cfg.REWRITE_SAVED_EMBEDDED_DATA:
                    embedded_query = np.load(query_path)

                else:
                    query_words = word_tokenize(" ".join(query))

                    print("words from query not found in vocab:", [word for word in query_words if word not in self.w2v.wv.vocab])
                    embedded_query = np.stack([self.embedd_query_word(word) for word in query_words])

                    np.save(query_path, embedded_query)

                embedded_queries.append(embedded_query)

            max_num_of_sentences = max(max(len(sentence_indices) for sentence_indices in docs_sentences_indices), cfg.SENTENCES_TO_RETRIEVE)

            # padd with -1
            docs_sentences_indices = [np.concatenate((sentence_indices, np.array([-1]*(max_num_of_sentences-len(sentence_indices))))) for sentence_indices in docs_sentences_indices]

            # padd queries
            max_num_of_words_in_query = max(len(em_q) for em_q in embedded_queries)
            padded_embedded_queries = [np.concatenate(
                (
                    em_q,
                    np.zeros(
                        (max_num_of_words_in_query-len(em_q),
                         self.sg_embedding_size)
                    )
                ))
                for em_q in embedded_queries]
            padded_embedded_queries = np.stack(padded_embedded_queries)


            # padd docs
            max_num_of_words = max(len(em_doc) for em_doc in embedded_docs)
            padded_embedded_docs = [np.concatenate(
                (
                    em_doc,
                    np.zeros(
                        (max_num_of_words-len(em_doc),
                         self.full_embedding_size)
                    )
                ))
                for em_doc in embedded_docs]

            padded_embedded_docs = np.stack(padded_embedded_docs)

            # sentences indices
            docs_sentences_indices = np.stack(docs_sentences_indices)


            # padd best indices
            assert len(batch_ids) == len(batch_best_indices)
            for index, ID in enumerate(batch_ids):

                bi_path = os.path.join(np_save_dir, "best_indices_" + str(ID) + ".npy")

                if os.path.isfile(bi_path) and not cfg.REWRITE_SAVED_EMBEDDED_DATA:
                    batch_best_indices[index] = np.load(bi_path)

                else:
                    assert batch_best_indices[index] is not None
                    np.save(bi_path, np.array(batch_best_indices[index]).astype(np.float32))

            max_len = max(max([len(inds) for inds in batch_best_indices]), cfg.SENTENCES_TO_RETRIEVE)

            padded_best_indices = np.array([np.concatenate(
                (
                    inds,
                    -1*np.ones(
                        (max_len-len(inds),)
                    )

                )
            ) for inds in batch_best_indices])

            # classes = np.array(batch_data.classes)

            feed_dict = {
                self.embedded_words: padded_embedded_docs,
                self.sentence_indices: docs_sentences_indices,
                self.vectorized_queries: padded_embedded_queries,
                self.best_indices: padded_best_indices,
                trainer.labels: classes,
                trainer.epoch: epoch
            }

        else:
            vectorized_text, vectorized_queries = tfidf_sentence_embedd(batch_data, self.vectorizer, batch_best_indices)
            feed_dict = {
                self.vectorized_text: vectorized_text,
                self.vectorized_queries: vectorized_queries,
                trainer.labels: np.array(batch_data.classes),
                trainer.epoch: epoch
            }

        assert isinstance(feed_dict, dict)
        return feed_dict


def expand_indices(indices, max_index):
    with tf.variable_scope("expand_indices"):
        indices = tf.cast((indices + max_index) % max_index, dtype=tf.int64)

        max_count = tf.cast(tf.shape(indices)[1], tf.int32)

        indices = tf.cast(tf.expand_dims(indices, -1), dtype=tf.int32)
        batch_size = tf.cast(tf.shape(indices)[0], tf.int64)

        sentence_counter = tf.expand_dims(
            tf.range(batch_size, dtype=tf.int64),
            1)

        tiler = tf.stack((1, max_count))

        sentence_counter = tf.tile(sentence_counter, tiler)
        sentence_counter = tf.cast(tf.reshape(sentence_counter, tf.shape(indices)), tf.int32)

        indices = tf.concat((sentence_counter, indices), axis=2)

    return indices


def tfidf_sentence_embedd(data, vectorizer, best_indices):
    vectorized_padded_docs = []
    vectorized_queries = []

    data = loader.DataHolder(texts=[[" ".join([token[0] for token in sentence])  for sentence in document] for document in data.texts], \
                queries=data.queries, \
                classes=data.classes)

    vectorized_data = vectorizer.process(data, model_phase=True)
    
    for i, (doc, query, cls) in enumerate(vectorized_data):
        vecto_sentences = []

        for j, vecto_sentence in enumerate(doc):
            if j in best_indices[i]:
                vecto_sentences.append(vecto_sentence)


        vectorized_doc = vstack(vecto_sentences)
        padded_doc = vstack([vectorized_doc,
            np.zeros((cfg.SENTENCES_TO_RETRIEVE-vectorized_doc.shape[0], cfg.TFIDF_SIZE))])

        # from csr to normal matrix
        vectorized_padded_docs.append(padded_doc.toarray())

        vectorized_queries.append(np.squeeze(query.toarray(), 0))

    return  np.array(vectorized_padded_docs), np.array(vectorized_queries)


def sentence_sentiment_embedding(vectorized_text, sentence_sentiments=None):
    # todo: mihaela
    # todo: sentence_sentiments sam mislio da su shape (batch_size, num_sentences, float?)
    # todo: vjv ce trebat onda to dodat u feed_dict, ugl ja bi to zadnje dodao
    return vectorized_text


def brc_sentence_embed(embedded_words, sentence_indices, kernel_size=5):

    with tf.variable_scope("brc_sentence_embed"):

        # embedded_words = tf.Print(embedded_words, [tf.shape(embedded_words)], message="embedded_words_shape")

        with tf.variable_scope("BiRNN"):
            lstm_fw_cell = rnn.BasicLSTMCell(cfg.BRC_BIRNN_HIDDEN_SIZE)
            lstm_bw_cell = rnn.BasicLSTMCell(cfg.BRC_BIRNN_HIDDEN_SIZE)

            (outputs_fw, outputs_bw), _ = tf.nn.bidirectional_dynamic_rnn(
                lstm_fw_cell, lstm_bw_cell, embedded_words, dtype=tf.float32)

        with tf.variable_scope("outputs_gathering"):

            def gather_for_indices(outputs, sentence_indices, embedded_words):
                with tf.variable_scope("gather_for_indices"):
                    max_index = tf.shape(embedded_words)[-2]
                    indices = expand_indices(sentence_indices, max_index)

                    gathered_outputs = tf.gather_nd(outputs, indices)

                return gathered_outputs
            gathered_fw_outputs = gather_for_indices(outputs_fw, sentence_indices, embedded_words)
            with tf.variable_scope("create_bw_indices"):
                # create bw_indices, we need ends of sentences
                batch_size = tf.cast(tf.shape(embedded_words)[0], tf.int64)

                bw_indices = sentence_indices[:, 1:]

                # column of -1
                # -1 is the index of the ending of the last sentence, we append it to the ending of every document
                last_ind_column = tf.transpose(tf.expand_dims(
                    tf.tile(
                        tf.cast([-1], dtype=tf.int32),
                        tf.cast(tf.expand_dims(batch_size, -1), dtype=tf.int32)
                    ),
                    axis=0))
                bw_indices = tf.concat((bw_indices, last_ind_column), axis=-1)
            gathered_bw_outputs = gather_for_indices(outputs_bw, bw_indices, embedded_words)

            # alternative way, exitig words
            # gathered_bw_outputs = gather_for_indices(outputs_bw, sentence_indices, embedded_words)
            # gathered_fw_outputs = gather_for_indices(outputs_fw, bw_indices, embedded_words)

            embedded_sentences = tf.concat((gathered_bw_outputs, gathered_fw_outputs), axis=-1)

        with tf.variable_scope("convolution"):

            embedded_sentences = tf.expand_dims(embedded_sentences, axis=-1)


            # SAME padding along second dimension, VALID along others

            if not cfg.REAL_CONVOLUTION:
                conv_filter = tf.get_variable(
                    name="brc_filter",
                    shape=[kernel_size, 2*cfg.BRC_BIRNN_HIDDEN_SIZE, 1, cfg.BRC_CONV_SIZE],
                    initializer=tf.random_normal_initializer
                )

                convolved_sentences = tf.nn.conv2d(
                    input=embedded_sentences,
                    filter=conv_filter,
                    padding="SAME",
                    strides=[1, 1, cfg.BRC_BIRNN_HIDDEN_SIZE*2, 1]
                )

            else:
                convolved_sentences = tf.layers.conv2d(
                    name="conv_layer",
                    inputs=embedded_sentences,
                    filters=cfg.BRC_CONV_SIZE,
                    kernel_size=[kernel_size, 2*cfg.BRC_BIRNN_HIDDEN_SIZE],
                    strides=[1, cfg.BRC_BIRNN_HIDDEN_SIZE*2],
                    padding="SAME",
                    activation=tf.nn.relu)
                convolved_sentences = tf.squeeze(convolved_sentences, 2)


        return convolved_sentences


def sentence_retrieve(embedded_sentences, best_indices):
    with tf.variable_scope("sentence_retrieve"):
        max_sentences_count = tf.cast(tf.shape(embedded_sentences)[1], tf.float32)

        best_indices = expand_indices(best_indices, max_sentences_count)

        vectorized_text = tf.gather_nd(embedded_sentences, best_indices)

        return vectorized_text


def text_convolution(input, representation_size=20, kernel_size=3, output_channels=200, max_pool_kernel_size=None,
                     name="text_convolution"):

    with tf.variable_scope(name):
        # SAME padding along second dimension, VALID along others
        net = input

        with tf.variable_scope("SAME_padding_pad"):
            net = tf.pad(net, [[0, 0], [kernel_size//2, kernel_size//2], [0, 0]])
            net = tf.expand_dims(net, axis=-1)

        net = tf.layers.conv2d(
            inputs=net,
            filters=output_channels,
            kernel_size=[kernel_size, representation_size],
            padding="VALID",
            activation=tf.nn.relu)

        net = tf.transpose(net, perm=[0, 1, 3, 2])

        if max_pool_kernel_size is not None:
            net = text_max_pool(net, kernel_size=max_pool_kernel_size)

        net = tf.squeeze(net, axis=-1)

        return net


def text_max_pool(net, kernel_size=3, name="Max_pool"):

    with tf.variable_scope(name):

        net = tf.nn.max_pool(
            net,
            padding="SAME",
            ksize=[1, kernel_size, 1, 1],
            strides=[1, kernel_size, 1, 1])

    return net


def classifier(vectorized_text, vectorized_queries, conv_sizes=[100], sentence_count=300, num_classes=9, brc=True):

    with tf.variable_scope("classifier"):
        if not brc:
            vectorized_text = tf.concat((vectorized_text, tf.expand_dims(vectorized_queries, 1)), axis=1)

        net = vectorized_text
        print("Classifier: ", cfg.CLASSIFIER)

        if cfg.CLASSIFIER == "LOGREG":
            net = tf.reshape(net, (-1, cfg.BRC_CONV_SIZE*sentence_count))
            net = tf.contrib.layers.fully_connected(net, num_outputs=num_classes, activation_fn=tf.identity)

        elif cfg.CLASSIFIER == "BRC":

            hidden_size = 200
            net = dense_brc_layer(net, hidden_brnn=hidden_size, hidden_c=hidden_size)
            net = tf.reshape(net, (-1, cfg.SENTENCES_TO_RETRIEVE*hidden_size))
            net = tf.contrib.layers.fully_connected(net, num_outputs=num_classes, activation_fn=tf.identity)

        elif cfg.CLASSIFIER == "CONV":


            max_pool_kernel_size = 3
            representation_size = cfg.BRC_CONV_SIZE if brc else cfg.TFIDF_SIZE

            # classaifier 1
            # net = text_convolution(net, output_channels=conv_sizes[0], name="conv_1", representation_size=representation_size, max_pool_kernel_size=max_pool_kernel_size)
            #
            # depth = int(np.ceil(sentence_count / max_pool_kernel_size))
            # net = tf.reshape(net, (-1, depth*conv_sizes[-1]))

            # classifier 2
            conv_sizes = [100, 200]

            net = text_convolution(net, output_channels=conv_sizes[0], name="conv_1", representation_size=representation_size,
                                   max_pool_kernel_size=None)

            net = text_convolution(net, output_channels=conv_sizes[1], name="conv_2", representation_size=conv_sizes[0],
                                   max_pool_kernel_size=None)

            depth = int(np.ceil(sentence_count / max_pool_kernel_size ** 1))
            depth = sentence_count
            net = tf.reshape(net, (-1, depth*conv_sizes[-1]))

            net = tf.contrib.layers.fully_connected(net, num_outputs=num_classes, activation_fn=tf.identity)

        else:
            raise ValueError


        return net


def dense_brc_layer(input, kernel_size=3, hidden_brnn=50, hidden_c=50, name="dense_brc_layer"):

    with tf.variable_scope(name):

        with tf.variable_scope("BiLSTM"):
            lstm_fw_cell = rnn.BasicLSTMCell(hidden_brnn)
            lstm_bw_cell = rnn.BasicLSTMCell(hidden_brnn)

            (outputs_fw, outputs_bw), _ = tf.nn.bidirectional_dynamic_rnn(
                lstm_fw_cell, lstm_bw_cell, input, dtype=tf.float32)

            output = tf.concat((outputs_bw, outputs_fw), axis=-1)

            output = tf.expand_dims(output, axis=-1)

        with tf.variable_scope("Convolution"):

            # todo: try this out with real conv
            conv_filter = tf.get_variable(
                name="brc_filter",
                shape=[kernel_size, 2*hidden_brnn, 1, hidden_c],
                initializer=tf.random_normal_initializer
            )


            output = tf.nn.conv2d(
                input=output,
                filter=conv_filter,
                padding="SAME",
                strides=[1, 1, hidden_brnn*2, 1]
            )

            output = tf.squeeze(output, 2)

        return output




