import tensorflow as tf
import argparse
import sys
import numpy as np
import os
import loader
import util
import collections
import model
import config
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
import config as cfg

class Trainer:

    def __init__(self, model: model.Model, retrieved_data: collections.namedtuple, experiment_name: str, lightweight=False):

        self.retrieved_data = retrieved_data
        self.model = model
        self.lightweight = lightweight

        self.epoch = tf.placeholder(tf.float32, name="epoch")
        tf.summary.scalar("epoch", self.epoch)

        with tf.variable_scope("loss"):
            self.labels = tf.placeholder(tf.int32, (None,), name="labels")
            self.loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
                labels=self.labels, logits=self.model.logits))
            tf.summary.scalar("loss:", self.loss)

        with tf.variable_scope("update_op"):
            self.global_step = tf.Variable(0, trainable=False)

            # starter_learning_rate = 0.001
            # self.learning_rate = tf.maximum(tf.train.exponential_decay(
            #     starter_learning_rate, self.global_step, decay_steps=100, decay_rate=0.85, staircase=True), 1e-6)
            starter_learning_rate = 0.0001
            self.learning_rate = tf.maximum(tf.train.exponential_decay(
                starter_learning_rate, self.global_step, decay_steps=300, decay_rate=0.99, staircase=True), 1e-6)

            tf.summary.scalar("learning rate", self.learning_rate)

            with tf.control_dependencies(tf.get_collection("assert_ops")):
                if cfg.CLIP:
                    self.optimizer = tf.train.AdamOptimizer(self.learning_rate)
                    gvs = self.optimizer.compute_gradients(self.loss)
                    capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in gvs]
                    self.optimize_step = self.optimizer.apply_gradients(capped_gvs, global_step=self.global_step)

                else:
                    self.optimize_step = tf.train.AdamOptimizer(self.learning_rate).minimize(
                        self.loss,
                        global_step=self.global_step)



        self.experiment_name = experiment_name
        self.experiment_dir = os.path.abspath(os.path.join("./experiments/", experiment_name))
        self.checkpoint_dir = os.path.abspath(os.path.join("./experiments/", experiment_name, "checkpoint/"))

        self.train_dir = '/'.join([self.experiment_dir, "summaries/train"])
        self.train_writer = tf.summary.FileWriter(self.train_dir)
        self.train_writer.add_graph(graph=tf.get_default_graph())

        self.test_dir = '/'.join([self.experiment_dir, "summaries/test"])
        self.test_writer = tf.summary.FileWriter(self.test_dir)
        self.merged = tf.summary.merge_all()

        self.saver = tf.train.Saver()


    def load_or_initialize(self, session):
        # incijalizacija parametara
        if not os.path.exists(self.checkpoint_dir):
            os.makedirs(self.checkpoint_dir)

        latest_checkpint_path = tf.train.latest_checkpoint(self.checkpoint_dir)

        if latest_checkpint_path and input("load " + latest_checkpint_path + " ? y/n ") == "y":
            print("Loading")
            self.saver.restore(session, latest_checkpint_path)
            begin_step = self.global_step.eval(session=session)

        else:

            session.run(tf.global_variables_initializer())
            begin_step = 0

        tf.get_default_graph().finalize()
        return begin_step


    def train(self):

        epoch = 1
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        config.gpu_options.per_process_gpu_memory_fraction = 0.9

        with tf.Session(config=config).as_default() as sess:
            begin_step = self.load_or_initialize(sess)
            epoch = (begin_step // (len(retrieved_data.train_data) // 20)) + 1

            for i in range(begin_step, 100000):

                # get batch from self.retrieved_data
                start_ix = int((i*cfg.batch_size) % (np.ceil(len(retrieved_data.train_data)/cfg.batch_size)*cfg.batch_size))
                print("i:", i, " start_ix:", start_ix)
                end_ix = start_ix+cfg.batch_size

                new_epoch = start_ix == 0

                if new_epoch and i > 0:
                    retrieved_data.shuffle()

                if not self.lightweight:
                    train_batch_data = retrieved_data.train_data[start_ix:end_ix]
                    train_batch_data = loader.DataHolder(*train_batch_data)
                else:
                    train_batch_data = loader.LightweightDataHolder(retrieved_data.train_len)

                train_batch_indices = retrieved_data.train_indices[start_ix:end_ix]
                train_batch_ids = retrieved_data.train_ids[start_ix:end_ix]

                train_feed_dict = self.model.embedd_and_create_feed_dict(
                    batch_data=train_batch_data,
                    batch_best_indices=train_batch_indices,
                    batch_ids=train_batch_ids,
                    trainer=self,
                    epoch=epoch,
                    train=True
                )

                if new_epoch and i > 0:
                    epoch += 1
                    print("epoch:", epoch)

                    # checkpoint
                    print("Saving")
                    print(self.checkpoint_dir+"/"+self.experiment_name)
                    self.saver.save(sess, self.checkpoint_dir+"/"+self.experiment_name, global_step=self.global_step)

                    summed_test_loss = 0
                    for e_iter in range(0, (len(retrieved_data.test_data)//cfg.batch_size)*cfg.batch_size, cfg.batch_size):
                        print("eval_iter:", e_iter)
                        # we omit the last batch that is smaller than cfg.batch_size

                        if not self.lightweight:
                            test_batch_data = retrieved_data.test_data[e_iter:e_iter+cfg.batch_size]
                            test_batch_data = loader.DataHolder(*test_batch_data)
                        else:
                            test_batch_data = loader.LightweightDataHolder(retrieved_data.test_len)

                        test_batch_indices = retrieved_data.test_indices[e_iter:e_iter+cfg.batch_size]
                        test_batch_ids = retrieved_data.test_ids[e_iter:e_iter+cfg.batch_size]

                        test_feed_dict = self.model.embedd_and_create_feed_dict(
                            batch_data=test_batch_data,
                            batch_best_indices=test_batch_indices,
                            batch_ids=test_batch_ids,
                            trainer=self,
                            epoch=epoch,
                            train=False
                        )

                        test_loss_e, test_summary, global_step_e = sess.run(
                            [
                                self.loss,
                                self.merged,
                                self.global_step
                            ],
                            feed_dict=test_feed_dict  # train batch
                        )

                        summed_test_loss += test_loss_e

                    averaged_test_loss = summed_test_loss / float(len(retrieved_data.test_data)//cfg.batch_size)

                    # training step
                    _, train_loss_e, learning_rate_e, train_summary, global_step_e2 = sess.run(
                        [
                            self.optimize_step,
                            self.loss,
                            self.learning_rate,
                            self.merged,
                            self.global_step
                        ],
                        feed_dict=train_feed_dict  # test batch
                    )

                    # assert global_step_e == global_step_e2

                    self.train_writer.add_summary(train_summary, global_step=epoch)
                    self.test_writer.add_summary(test_summary, global_step=epoch)

                    print()
                    print("Learning_rate:", learning_rate_e)
                    print("Test loss:", averaged_test_loss)
                    print("Train loss:", train_loss_e)
                    print()

                else:
                    sess.run(
                        [
                            self.optimize_step,
                        ],
                        feed_dict=train_feed_dict  # train batch
                    )



def load_classes(variations_path):
    with open(variations_path) as variations_file:
        classes_names = [var.rstrip().split(",")[-1] for var in variations_file]  # extract classes from file
        classes = list(map(lambda x: int(x)-1, classes_names))

    return classes


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--exp_name', default="test")
    parser.add_argument('--lw', action='store_true', default=False)
    args = parser.parse_args()
    experiment_name = args.exp_name
    lw = args.lw

    if not lw:
        if os.path.isfile(cfg.TRAIN_PREPROCESSED_PATH) and os.path.isfile(cfg.TEST_PREPROCESSED_PATH):
            print("Loading preprocessed")

            with open(cfg.TEST_PREPROCESSED_PATH, 'rb') as f:
                test_data = pickle.load(f)

            with open(cfg.TRAIN_PREPROCESSED_PATH, 'rb') as f:
                train_data = pickle.load(f)

        else:

            train_data = loader.DataHolder()
            test_data = loader.DataHolder()

            train_data.loadData(cfg.TRAIN_TEXT_PATH, cfg.TRAIN_VARIANTS_PATH)
            test_data.loadData(cfg.TEST_TEXT_PATH, cfg.TEST_VARIANTS_PATH)


            print("Data loaded")
            print("Preprocessing")

            tokenizer = util.Tokenizer()
            pos_tagger = util.POSTagger()
            lemmatizer = util.Lematizer()
            alias_supstitutor = util.AliasSubstitutor()
            query_expander = util.QueryExpander()

            preprocessor = util.Preprocessor([alias_supstitutor, tokenizer, pos_tagger, lemmatizer, query_expander])

            train_data = preprocessor.process(train_data)
            test_data = preprocessor.process(test_data)


            with open(cfg.TEST_PREPROCESSED_PATH, 'wb') as f:
                pickle.dump(test_data, f)

            with open(cfg.TRAIN_PREPROCESSED_PATH, 'wb') as f:
                pickle.dump(train_data, f)

        if os.path.isfile(cfg.TF_IDF_VECTORIZED_PATH):
            with open(cfg.TF_IDF_VECTORIZED_PATH, "rb") as f:
                vectorizer = pickle.load(f)
                floki = util.Retriever(vectorizer)

        else:
            vectorizer = util.TfidfVectorizer(train_data)  # creates and fits vectorizer
            floki = util.Retriever(vectorizer)

            with open(cfg.TF_IDF_VECTORIZED_PATH, "wb") as f:
                pickle.dump(vectorizer, f)

        print("Retrieving")
        train_indices = floki.retrieve(train_data, sentence_count=cfg.SENTENCES_TO_RETRIEVE)
        test_indices = floki.retrieve(test_data, sentence_count=cfg.SENTENCES_TO_RETRIEVE)

        retrieved_data = util.RetrievedData(
            train_data=train_data, train_indices=train_indices,
            test_data=test_data, test_indices=test_indices)

    else:
        print("Lightweight mode on")
        retrieved_data = util.LightweightRetrievedData(train_len=3321, test_len=368)
        vectorizer = None  # for brc

    types = [loader.DataHolder, list, loader.DataHolder, list]

    print("Preprocessing done.")

    model = model.Model(vectorizer=vectorizer, brc_embedding=cfg.BRC_EMBEDDING, sentences_count=cfg.SENTENCES_TO_RETRIEVE)
    trainer = Trainer(model, retrieved_data=retrieved_data, experiment_name=experiment_name, lightweight=lw)

    trainer.train()

