from abc import abstractmethod
import numpy as np
import loader
import collections
import itertools
from typing import List
from sklearn.feature_extraction.text import TfidfVectorizer as sk_tfidfvectorizer
import config as cfg

import sklearn.metrics
from scipy.sparse import csr_matrix
from scipy.sparse import vstack
from scipy.sparse import hstack

import random
from nltk import wordpunct_tokenize
from nltk import pos_tag
from nltk import WordNetLemmatizer
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords as sw
import string
import codecs

import config as cfg

# RetrievedData = collections.namedtuple("RetrievedData", "train_data train_indices test_data test_indices")

class RetrievedData:

    def __init__(self, train_data, train_indices, test_data, test_indices):

        self.train_data = train_data
        self.train_indices = train_indices
        self.train_ids = list(range(len(train_data)))

        self.test_data = test_data
        self.test_indices = test_indices
        self.test_ids = list(range(len(test_data)))

    def shuffle(self):
        random.shuffle(self.train_ids)
        self.train_data.shuffle(self.train_ids)
        self.train_indices = [self.train_indices[ind] for ind in self.train_ids]


class LightweightRetrievedData(RetrievedData):

    def __init__(self, train_len, test_len):
        RetrievedData.__init__(self,
                               loader.LightweightDataHolder(train_len),
                               [None]*train_len,
                               loader.LightweightDataHolder(test_len),
                               [None]*test_len)
        self.train_len = train_len
        self.test_len = test_len

    def shuffle(self):
        random.shuffle(self.train_ids)




class Parser:

    def __init__(self):
        pass

    def process(self, data: loader.DataHolder):
        # TODO
        return data

class Tokenizer(Parser):

    def __init__(self):
        Parser.__init__(self)
        self.lower = True
        self.strip = True
        self.stopwords = set(sw.words('english'))
        self.punct = set(string.punctuation)

    def process(self, data: loader.DataHolder):
        # Break the sentence into part of speech tagged tokens
        print("tokenizing")

        new = []
        texts = data.texts
        for document in texts:
            new_texts = []
            for sentence in document:
                tokens = []

                for token in (wordpunct_tokenize(sentence)):
                    # Apply preprocessing to the token
                    token = token.lower() if self.lower else token
                    token = token.strip() if self.strip else token
                    token = token.strip('_') if self.strip else token
                    token = token.strip('*') if self.strip else token
                    # if  all chars from token are in string.punctuation

                    if all(char in self.punct for char in token):
                        continue

                    tokens.append(token)

                new_texts.append(tokens)

            new.append(new_texts)

        data.texts = new
      

        return data

class POSTagger (Parser):

    def __init__(self):
        Parser.__init__(self)

    def process (self, data: loader.DataHolder):
        print("pos tagger")
        docs = data.texts
        
        texts = []
        
        for i, doc in enumerate(docs):
            print("document ", i, " is being tagged")
            new_text = []
            
            for sent in doc:
                tuples = []
                
                for token, tag in pos_tag(sent):
                    tuples.append((token, tag))
                new_text.append(tuples)

            texts.append(new_text)
        data.texts = texts

        
        return data


class Lematizer(Parser):

    def __init__(self):
        Parser.__init__(self)
        self.lemmatizer = WordNetLemmatizer()

    def process(self, data: loader.DataHolder):
        print("Lemmatizing")
        docs = data.texts

        texts = []

        for i, doc in enumerate(docs):
            print("lemmatizing {}/{}".format(i, len(docs)))
            new_text = []

            for sent in doc:
                lemmas = []

                for token, tag in sent:
                    old_tag = tag
                    tag = {
                          'N': wn.NOUN,
                          'V': wn.VERB,
                          'R': wn.ADV,
                          'J': wn.ADJ
                          }.get(tag[0], wn.NOUN)

                    lemma = self.lemmatizer.lemmatize(token, tag)

                    lemmas.append((lemma, old_tag))

                new_text.append(lemmas)

            texts.append(new_text)

        data.texts = texts
        

        new_queries = []
        for q in data.queries:
            q_words = list(itertools.chain([q[0]], q[1].split()))
            lemmas = []

            for token, tag in pos_tag(q_words):
                tag = {
                      'N': wn.NOUN,
                      'V': wn.VERB,
                      'R': wn.ADV,
                      'J': wn.ADJ
                       }.get(tag[0], wn.NOUN)

                lemma = self.lemmatizer.lemmatize(token, tag)

                lemmas.append(lemma)

            new_queries.append([lemmas[0], " ".join(lemmas[1:])])

        data.queries = new_queries

        return data


class AliasSubstitutor(Parser):

    def __init__(self):
        Parser.__init__(self)

    def process(self, data: loader.DataHolder):
        print("alias_substitutor")
        
        alias_dict = return_alias_dictionary()

        for i in range(len(data.texts)):

            gene = data.queries[i][0]
            aliases = alias_dict.get(gene, [])

            if gene not in alias_dict:
                print("Gene not in dict: ", gene)

            
            for a in aliases:

                for j in range(len(data.texts[i])):
                    
                    data.texts[i][j] = data.texts[i][j].replace(a.lower(), ' ' + gene + ' ')

        
        return data

class QueryExpander(Parser):

    def __init__(self):
        Parser.__init__(self)

    def process(self, data: loader.DataHolder):
        print("query expander")
        data.queries = [q+cfg.QUERY_EXPANSION_ELEMENTS for q in data.queries]

        return data


class TfidfVectorizer(Parser):

    def __init__(self, data):
        Parser.__init__(self)
        self.sk_vectorizer = sk_tfidfvectorizer(min_df=cfg.MINIMAL_DF)
        sentences = list(itertools.chain(*data.texts))
        self.sk_vectorizer.fit(sentences)

    def process(self, data: loader.DataHolder, model_phase=False):
        print("tfidf vectorizing")
        if model_phase:
            vec_text = [self.sk_vectorizer.transform([" ".join([word for word in sent]) for sent in text]) for text,_,_ in data]
        else:
            vec_text = [self.sk_vectorizer.transform([" ".join([word for word, _ in sent]) for sent in text]) for text,_,_ in data]


        queries = [" ".join(q) for q in data.queries]
        vec_queries = self.sk_vectorizer.transform(queries)

        data = loader.DataHolder(texts=vec_text, queries=vec_queries, classes=data.classes)
        return data


class Preprocessor(Parser):

    def __init__(self, parsers: List[Parser]):
        Parser.__init__(self)
        self.parsers = parsers

    def process(self, data: loader.DataHolder):
        for parser in self.parsers:
            data = parser.process(data)

        return data


class Retriever:

    def __init__(self, vectorizer):
        self.vectorizer = vectorizer

    def retrieve(self, data: loader.DataHolder, sentence_count=300):

        def retrieve_from_vectorized(documents_v, queries_v, sentence_count):
            # todo franko: add Gaussian retrieval

            indices = []
            for d, q in zip(documents_v, queries_v):

                if d.shape[0] > sentence_count:
                    sims = sklearn.metrics.pairwise.cosine_similarity(d, q).T[0]
                    ind = list(map(int, np.argpartition(sims, sentence_count)))[-sentence_count:]

                else:
                    ind = list(range(d.shape[0]))

                ind = sorted(ind)
                indices.append(ind)

            return indices

        data_vectorized = self.vectorizer.process(data)
        return retrieve_from_vectorized(data_vectorized.texts, data_vectorized.queries, sentence_count=sentence_count)


def return_alias_dictionary():

    alias_dict = {}

    with open(cfg.ALIASES, "r") as f:

        for line in f:

            columns = line.split('\t')

            aliases = []

            columns[2] = columns[2].replace('"', "")
            aliases.extend(columns[2].split("|"))

            columns[8] = columns[8].replace('"', "")
            columns[9] = columns[9].replace('"', "")
            columns[10] = columns[10].replace('"', "")
            columns[11] = columns[11].replace('"', "")

            alias_symbols = columns[8].split("|")
            aliases.extend(alias_symbols)

            alias_names = columns[9].split("|")
            aliases.extend(alias_names)

            prev_symbols = columns[10].split("|")
            aliases.extend(prev_symbols)

            prev_names = columns[11].split("|")
            aliases.extend(prev_names)

            aliases = [x for x in aliases if x != '']
            alias_dict[columns[1]] = aliases

    return alias_dict
